FROM python:3.7-alpine
RUN apk update \
    && apk add python3 postgresql-libs \
    && apk add alpine-sdk python3-dev musl-dev postgresql-dev libffi-dev \
    && pip install -U setuptools pip
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
WORKDIR /usr/src/app
COPY . .
RUN pip install .
EXPOSE 80
ENTRYPOINT ["uvicorn", "hn_mirror.main:app", "--host=0.0.0.0", "--port=50001"]
