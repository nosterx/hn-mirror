#### Описание
Простое приложение которое парсит главную HackerNews раз в пятнадцать минут и предоставляет апишку отдающую результаты парсинга.
[Доступно онлайн](https://hn-mirror.herokuapp.com/posts/)
[Доки к апишке находятся здесь](https://hn-mirror.herokuapp.com/docs)

#### Посмотреть coverage + mypy + pylint
* git clone
* cd hn-mirror
* pipenv shell
* pipenv install --dev
* tox

#### Локальный запуск
* Установить docker и docker-compose
* git clone
* Выполнить cd hn-mirror && ./start.sh
* [Перейти автогенеренную документацию](http://127.0.0.1:50001/docs)

#### Запуск апи-тестов
* Выполнить пункт "Локальный запуск"
* pipenv shell
* pipenv install --dev
* tox -e api
