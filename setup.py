from os.path import (
    dirname,
    join,
)

from setuptools import (
    find_packages,
    setup,
)


setup(
    name='hn_mirror',
    version='0.1.0',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    long_description=open(
        join(dirname(__file__), 'README.md')
    ).read(),
    entry_points={
        'console_scripts':
            ['hn-mirror = hn_mirror.cli:cli']
    },
)
