import asyncio

import click
import sqlalchemy as sa
import uvicorn

from hn_mirror.db import DATABASE_URL
from hn_mirror.pg_tables import metadata
from hn_mirror.services import manual_update


@click.group()
def cli() -> None:
    ...

@cli.command()
@click.option('--reload', is_flag=False)
@click.option('--host', type=str, default='127.0.0.1')
@click.option('--port', type=int, default=8000)
@click.option(
    '--log-level',
    type=click.Choice(['info', 'debug', 'error'], case_sensitive=False),
    default='info',
)
def serve(reload: bool, host: str, port: int, log_level=str) -> None:
    uvicorn.run('hn_mirror.main:app', host=host, port=port, log_level=log_level, reload=reload)


@cli.command()
def create_tables():
    engine = sa.create_engine(str(DATABASE_URL))
    metadata.create_all(engine)


@cli.command()
def update_posts():
    asyncio.run(manual_update())
