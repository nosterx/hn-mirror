from datetime import datetime

from pydantic import BaseModel


class PostBase(BaseModel):
    id: int
    title: str
    url: str


class Post(PostBase):
    created_at: datetime


class PostCreate(PostBase):
    ...
