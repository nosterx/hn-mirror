from enum import Enum


class OrderBy(str, Enum):
    id = 'id'
    url = 'url'
    title = 'title'  # type: ignore
    created_at = 'date'


class Order(str, Enum):
    asc = 'asc'
    desc = 'desc'
