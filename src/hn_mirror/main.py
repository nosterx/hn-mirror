from typing import List

import aiocron
from fastapi import (
    FastAPI,
    Query,
)

from hn_mirror.db import database
from hn_mirror.entities import Post
from hn_mirror.enums import (
    Order,
    OrderBy,
)
from hn_mirror.repo import filter_posts
from hn_mirror.services import update


INT64_MAX = 2 ** 64 // 2 -1


app = FastAPI()

@app.on_event('startup')
async def startup() -> None:
    await database.connect()
    aiocron.crontab('*/15 * * * *', func=update)


@app.on_event('shutdown')
async def shutdown() -> None:
    await database.disconnect()


@app.get(
    '/posts/',
    response_model=List[Post],
)
async def get_posts(
        *,
        offset: int = Query(0, ge=0, le=INT64_MAX),
        limit: int = Query(5, ge=5, le=500),
        order: Order = Order.asc,
        order_by: OrderBy = OrderBy.created_at,
):
    return await filter_posts(
        order=order,
        order_by=order_by,
        offset=offset,
        limit=limit,
    )
