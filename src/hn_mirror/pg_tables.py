import sqlalchemy as sa
from sqlalchemy.sql import func


metadata = sa.MetaData()


posts = sa.Table(
    'posts', metadata,
    sa.Column('id', sa.INTEGER, primary_key=True),
    sa.Column('title', sa.TEXT, nullable=False),
    sa.Column('url', sa.TEXT, nullable=False),
    sa.Column('created_at', sa.TIMESTAMP(timezone=True), nullable=False, default=func.now()),
)
