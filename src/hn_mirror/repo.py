from typing import (
    Any,
    List,
    Mapping,
)

import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import insert

from hn_mirror.db import database
from hn_mirror.entities import PostCreate
from hn_mirror.enums import (
    Order,
    OrderBy,
)
from hn_mirror.pg_tables import posts as posts_table


async def filter_posts(
        *,
        order: Order,
        order_by: OrderBy,
        offset: int,
        limit: int,
) -> List[Mapping[Any, Any]]:
    order_by_clause = f'{order_by.name} {order.value}'
    query = (
        sa
        .select([posts_table])
        .order_by(sa.text(order_by_clause))
        .offset(offset)
        .limit(limit)
    )
    return await database.fetch_all(query)


async def save_many(posts: List[PostCreate]) -> None:
    query = (
        insert(posts_table)
        .values([post.dict() for post in posts])
        .on_conflict_do_nothing(index_elements=['id'])
    )
    await database.execute(query)
