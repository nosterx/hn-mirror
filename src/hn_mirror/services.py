import logging

import httpx
from bs4 import BeautifulSoup

from hn_mirror.db import database
from hn_mirror.entities import PostCreate
from hn_mirror.repo import save_many


logger = logging.getLogger(__name__)


async def update() -> None:
    try:
        async with httpx.AsyncClient() as client:
            response = await client.get('https://news.ycombinator.com/')
    except httpx.exceptions.HTTPError:
        logger.exception('Failed to download new posts')
        return

    soup = BeautifulSoup(response.content, features='html.parser')
    stories = soup.select('.athing')

    posts = []
    for story in stories:
        link = story.select_one('.storylink')
        posts.append(
            PostCreate(
                id=story['id'],
                title=link.text,
                url=link['href'],
            )
        )

    await save_many(posts)


async def manual_update() -> None:
    await database.connect()
    await update()
    await database.disconnect()
