#! /bin/bash
pipenv lock --requirements > requirements.txt \
    && sed --in-place -E '/^-e[[:space:]]+[.]/d' requirements.txt \
    && sudo docker-compose down \
    && sudo docker-compose up -d --build \
    && sudo docker exec hn_mirror_api hn-mirror create-tables && hn-mirror update-posts \
    && rm requirements.txt
