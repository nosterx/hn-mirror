import httpx
import pytest
from mock import MagicMock


class AsyncContextManagerMock(MagicMock):
    async def __aenter__(self):
        return self

    async def __aexit__(self, *args):
        pass


@pytest.fixture(name='response')
def response_fixture(mocker):
    return mocker.Mock(spec=httpx.models.AsyncResponse)


@pytest.fixture(name='async_client')
def async_client_mock():
    client_mock = AsyncContextManagerMock()
    return client_mock
