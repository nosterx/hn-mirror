from pathlib import Path

import httpx
import pytest

from hn_mirror.entities import PostCreate
from hn_mirror.services import update
from tests.utils import future


@pytest.mark.asyncio
class TestUpdate:
    async def test_call(self, mocker, response, async_client):
        # arrange
        content_file_path = Path(__file__).parent / 'data' / 'content.html'
        with open(content_file_path, 'r') as f:
            response.content = f.read()

        mocker.patch(
            'hn_mirror.services.httpx.AsyncClient',
            return_value=async_client,
        )
        async_client.get.return_value = future(response)

        save_many_mock = mocker.patch(
            'hn_mirror.services.save_many',
            return_value=future()
        )

        expected = [
            PostCreate(
                id=21490849,
                title='LineageOS Android Distribution',
                url='https://lineageos.org/',
            ),
            PostCreate(
                id=21490654,
                title='Show HN: Go Micro – A Go microservices development framework',
                url='https://go.micro.mu',
            ),
            PostCreate(
                id=21489651,
                title='Building a keyboard from an old typewriter',
                url='http://cowlark.com/2019-11-03-keyboard/',
            )
        ]

        # act
        await update()

        # assert
        save_many_mock.assert_called_once_with(expected)
        async_client.get.assert_called_once_with('https://news.ycombinator.com/')

    async def test_call__network_error__log_exception(self, mocker, async_client, caplog):
        # arrange
        mocker.patch(
            'hn_mirror.services.httpx.AsyncClient',
            return_value=async_client,
        )
        async_client.get.return_value = future(
            exception=httpx.exceptions.ReadTimeout()
        )

        save_many_mock = mocker.patch(
            'hn_mirror.services.save_many',
        )

        # act
        await update()

        # assert
        assert 'Failed to download new posts' in caplog.text
        save_many_mock.assert_not_called()
        async_client.get.assert_called_once_with('https://news.ycombinator.com/')
