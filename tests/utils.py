from asyncio import Future
from typing import Any


def future(value: Any = None, exception: Exception = None) -> Future:
    f = Future()
    if exception:
        f.set_exception(exception)
    else:
        f.set_result(value)
    return f
